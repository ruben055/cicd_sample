include {
  path = find_in_parent_folders()
}

terraform {
  source = "git@github.com:finaccel/terraform-modules-aws-iam.git//modules/user?ref=v1.1.1"
}

inputs = {
  environment   = "production"
  account_group = "engineering"
  squad         = "ux"

  name = basename(get_terragrunt_dir())
  groups = [
    "KredivoAssetsManager",
  ]

  policies_arn = []

  granted_roles_arn = []
}
